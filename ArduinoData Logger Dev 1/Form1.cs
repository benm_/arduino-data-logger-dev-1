﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Windows.Forms.DataVisualization.Charting;

namespace ArduinoData_Logger_Dev_1
{
    public partial class Form1 : Form
    {
        private SerialPort arduinoPort;
        private Dictionary<int, Series> seriesDict;
        private int packetsRec = 0;

        delegate void addYToSeriesCallback(double y, int sindex);
        delegate void setNameToSeriesCallback(string name, int sindex);

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            seriesDict = new Dictionary<int, Series>();
        }

        //COM PORT COMBO
        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            foreach (String s in SerialPort.GetPortNames())
            {
                comboBox1.Items.Add(s);
            }
        }

        //CONNECT BUTTON
        private void connectBtn_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex < 0)
            {
                MessageBox.Show("Please select a COM port!");
            }
            else if (comboBox2.SelectedIndex < 0)
            {
                MessageBox.Show("Please select a baud rate!");
            }
            else
            {
                string portname = (string)comboBox1.Items[comboBox1.SelectedIndex];
                int baudrate = new int[]{1200,2400,4800,9600,14400,19200,38400,57600,115200}[comboBox2.SelectedIndex];
                try
                {
                    arduinoPort = new SerialPort(portname, baudrate);
                    arduinoPort.DtrEnable = true;
                    arduinoPort.DataReceived += new SerialDataReceivedEventHandler(arduinoport_dataRec);
                    arduinoPort.Open();
                    connectBtn.Enabled = false;
                    disconnectBtn.Enabled = true;
                    seriesDict.Clear();
                    chart1.Series.Clear();
                    checkedListBox1.Items.Clear();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }



        }

        //DATA RECIEVED
        private void arduinoport_dataRec(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                string line = arduinoPort.ReadLine();
                if (line.Length > 2)
                {
                    char c1 = line[0];                  

                    switch (c1){
                        // ADD a point | syntax Ax-12.34 where x is the series number
                        case 'A':
                            int x1 = (line[1]-48)*10+(line[2]-48);          //index
                            double d = Double.Parse(line.Substring(3));     //value

                            addYToSeries(d, x1);

                            break;
                        // NAME a series | syntax NxSeriesName where x is the series number
                        case 'N':
                            int x2 = (line[1]-48)*10+(line[2]-48);          //index
                            string name = line.Substring(3);                //new series name

                            setNameToSeries(name, x2);

                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void createSeries(int index)
        {
            Series s = new Series();
            s.ChartType = SeriesChartType.Line;
            s.XValueType = ChartValueType.DateTime;
            s.YValueType = ChartValueType.Double;
            s.Name = "Series " + index;
            seriesDict[index] = s;            
        }

        private void updateCheckListBoxItems()
        {
            //update names
            foreach (string s in checkedListBox1.Items)
            {
                int index = int.Parse(s.Split(' ')[0]);
                if (seriesDict.ContainsKey(index))
                {
                    checkedListBox1.Items[index] = "" + index + " : " + seriesDict[index].Name;
                }
            }

            //add nonexistant series
            foreach (int k in seriesDict.Keys)
            {
                Series s = seriesDict[k];
                if (!checkedListBox1.Items.Contains("" + k + " : " + s.Name))
                {
                    checkedListBox1.Items.Add("" + k + " : " + s.Name, false);
                }
            }
        }

        //ADD A NEW DATAPOINT TO A SERIES
        private void addYToSeries(double y, int sindex)
        {
            if (chart1.InvokeRequired)
            {
                addYToSeriesCallback cb = new addYToSeriesCallback(addYToSeries);
                this.Invoke(cb, new object[] {y, sindex });
            }
            else
            {               
                double dn = (DateTime.Now).ToOADate();
                packetsRec++;
                toolStripStatusLabel1.Text = "Data Points Received: " + packetsRec;
                if (seriesDict.ContainsKey(sindex))
                {                    
                    seriesDict[sindex].Points.AddXY(dn, y);
                }
                else
                {
                    createSeries(sindex);
                    updateCheckListBoxItems();
                    seriesDict[sindex].Points.AddXY(dn, y);
                }                      
            }
        }

        //SET THE SERIES NAME OF SINDEX
        private void setNameToSeries(string name, int sindex)
        {
            if (chart1.InvokeRequired)
            {
                setNameToSeriesCallback cb = new setNameToSeriesCallback(setNameToSeries);
                this.Invoke(cb, new object[] { name, sindex });
            }
            else
            {
                double dn = (DateTime.Now).ToOADate();
                if (seriesDict.ContainsKey(sindex))
                {
                    seriesDict[sindex].Name = name;
                }
                else
                {
                    createSeries(sindex);
                    seriesDict[sindex].Name = name;
                }
                updateCheckListBoxItems();
            }
        }

        private void checkedListBox1_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void disconnectBtn_Click(object sender, EventArgs e)
        {
            if (arduinoPort.IsOpen)
            {
                arduinoPort.Close();
                connectBtn.Enabled = true;
                disconnectBtn.Enabled = false;
            }
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            chart1.Series.Clear();           
            

            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                if (e.Index == i)
                {
                    if (!checkedListBox1.GetItemChecked(i))
                    {
                        string s = checkedListBox1.Items[i].ToString();
                        int index = int.Parse(s.Split(' ')[0]);
                        chart1.Series.Add(seriesDict[index]);
                    }
                }
                else
                {
                    if (checkedListBox1.GetItemChecked(i))
                    {
                        string s = checkedListBox1.Items[i].ToString();
                        int index = int.Parse(s.Split(' ')[0]);
                        chart1.Series.Add(seriesDict[index]);
                    }
                }

                
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this.Text + ": Developed by Ben Meier (2012) to log data from an Arduino over USB.");
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {
            MessageBox.Show("Help:\n" +
                            "- \"Axx-12.345\\n\" : Log the value -12.345 to the data series xx\n" +
                            "- \"NxxGraph 1\\n\" : Set the name of the data series xx to \"Graph 1\"\n");
        }
    }
}
